// See https://dart.academy/structural-design-patterns-for-dart-and-flutter-composite/

class Item {
  final double price;
  final double weight;

  Item(this.price, this.weight);
}

class Container implements Item {
  final List<Item> items = [];
  void addItem(Item item) => items.add(item);
  @override
  double get price =>
      items.fold(0, (double sum, Item item) => sum + item.price);

  @override
  double get weight =>
      items.fold(0, (double weight, Item item) => weight + item.weight);
}

void main(List<String> arguments) {
  final container1 = Container()
    ..addItem(Item(5.95, 1.5))
    ..addItem(Item(9.99, 2))
    ..addItem(Item(25, 2.3));

  final container2 = Container()..addItem(Item(16.5, 9));

  container1.addItem(container2);

  print('Price: ${container1.price}');
  print('Weight: ${container1.weight}');

  // Output should be 57.44 for price and 14.8 for weight
  // Even though container1 has 3 Items and 1 Container,
  // The fold function is "deep"
}
