import 'dart:io';

void main() {
  print('OS: ${Platform.operatingSystem} ${Platform.operatingSystemVersion}');

  if (Platform.isLinux) {
    print('You are runnign a Linux system');
  } else if (Platform.isWindows) {
    print('Aha! Windows!');
  } else if (Platform.isMacOS) {
    print('Mac is kind fun!');
  } else if (Platform.isAndroid) {
    print('Android, hmm');
  }

  print('Path: ${Platform.script.path}');
  print('Dart: ${Platform.executable}');

  print('Env: ');
  for (var e in Platform.environment.keys) {
    print('$e = ${Platform.environment[e]}');
  }
}
