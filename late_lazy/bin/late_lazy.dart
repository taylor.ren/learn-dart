int _computeValue() {
  print('In _computeValue...');
  return 3;
}

class CachedValueProvider {
  late final _cache = _computeValue();

  //Try remove 'late' from above and see the output
  int get value => _cache;
}

void main() {
  print('Calling constructor');
  var provider = CachedValueProvider();

  print('Getting value...');
  print('The value is ${provider.value}');
}
