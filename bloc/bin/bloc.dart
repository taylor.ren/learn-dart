Stream<int> dataStream() async* {
  for (int i = 1; i <= 4; i++) {
    print('SENDING data $i');
    await Future.delayed(Duration(seconds: 2));
    yield i;
  }
}

void main() async {
  Stream<int> stream = dataStream();
  stream.listen((event) {
    print("RECEIVE data ${event.toInt()}");
  });
}
