import 'dart:io';
import 'dart:convert';

void main() {
  Process.start('dir', []).then((Process process) {
    //Transform the output
    process.stdout.transform(Utf8Decoder()).listen((data) => print(data));

    //Send to the process
    process.stdin.writeln('Hello World!');

    //Kill the process
    Process.killPid(process.pid);

    //Get the exit code
    process.exitCode.then(
      (value) => print('Exit Code $value'),
    );

    //exit
    exit(0);
  });
}
