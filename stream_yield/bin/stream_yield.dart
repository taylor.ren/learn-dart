// See https://dart.academy/streams-and-sinks-in-dart-and-flutter/

import 'dart:math';

Stream<int> count(int countTo) async* {
  for (int i = 1; i <= countTo; i++) {
    var duration = Random().nextInt(1000);
    yield duration;
    await Future.delayed(Duration(milliseconds: duration));
  }
}

void main(List<String> arguments) {
  count(11).listen((int value) {
    print('Value received: $value delay');
  });
}
