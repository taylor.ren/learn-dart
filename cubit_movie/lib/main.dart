import 'package:cubit_movie/src/cubit/movie_cubit.dart';
import 'package:cubit_movie/src/cubit/movie_repository.dart';
import 'package:cubit_movie/src/movie_page.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Movie Cubit Demo",
      theme: ThemeData(
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: BlocProvider<MovieCubit>(
        create: (context) => MovieCubit(
            repository: MovieRepository(
          Dio(),
        )),
        child: SizedBox (
          height: 500,
          child: MoviesPage(),
          ),
      ),
    );
  }
}
