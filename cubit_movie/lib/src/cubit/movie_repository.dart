import 'dart:convert';

import 'package:cubit_movie/src/cubit/movie_model.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;

class MovieRepository {
  const MovieRepository(this.client);

  final Dio client;

  Future<List<MovieModel>> getMovies() async {
    try {
      //const uri =
      'https://api.themoviedb.org/3/trending/movie/week?api_key=060e7c76aff06a20ca4a875981216f3f';

      const uri = 'https://api.rsywx.com/book/random/5';

      //final response = await client.get(uri);

      final response = await http.get(Uri.parse(uri));
      var res=jsonDecode(response.body);
      final movies = List<MovieModel>.of(
        res['data'].map<MovieModel>(
          (json) => MovieModel(
            title: json['title'],
            urlImage:
                'https://api.rsywx.com/book/image/${json['bookid']}/${json['bookid']}/${json['bookid']}/300',
          ),
        ),
      );
      return movies;
    } catch (e) {
      rethrow;
    }
  }
}
