import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import 'movie_repository.dart';
import 'movie_model.dart';

part 'movie_state.dart';



class MovieCubit extends Cubit<MovieState> {
  MovieCubit({required this.repository}) : super(InitialState()) {
    getRandomBooks();
  }

  final MovieRepository repository;
  void getRandomBooks() async {
    try {
      emit(LoadingState());
      final movies = await repository.getMovies();
      emit(LoadedState(movies));
    } catch (e) {
      emit(ErrorState());
    }
  }
}
