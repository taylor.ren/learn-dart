// See https://dart.academy/structural-design-patterns-for-dart-and-flutter-decorator/
/*Once again, we use an abstract class to define the interface all shape decorators should adhere to.
 Additionally, ShapeDecorator implements Shape, so all classes that extend ShapeDecorator are interface compatible with shape classes.
 This is key, because it means we can instantiate a decorator, pass it a shape, and then use the decorator as though it were the shape. That's the essence of the Decorator pattern.
*/

abstract class Shape {
  String draw();
}

class Square implements Shape {
  @override
  String draw() => "Square";
}

class Triangle implements Shape {
  @override
  String draw() => 'Trangle';
}

abstract class ShapeDecorator implements Shape {
  final Shape shape;
  ShapeDecorator(this.shape);
  @override
  String draw();
}

class GreenShapeDecorator extends ShapeDecorator {
  GreenShapeDecorator(Shape shape) : super(shape);

  @override
  String draw() => 'Green ${shape.draw()}';
}

class BlueShapeDecorator extends ShapeDecorator {
  BlueShapeDecorator(Shape shape) : super(shape);

  @override
  String draw() => 'Blue ${shape.draw()}';
}

void main(List<String> arguments) {
  final square = Square();
  print(square.draw());

  final greenSquare = GreenShapeDecorator(square);
  print(greenSquare.draw());

  final blueGreenSquare = BlueShapeDecorator(greenSquare);
  print(blueGreenSquare.draw());
}
