import 'dart:convert';

void main(List<String> arguments) {
  final IPostsAPI api1 = Site1Adapter();
  final IPostsAPI api2 = Site2Adapter();

  final List<Post> posts = api1.getPosts() + api2.getPosts();

  for (Post post in posts) {
    print('Title: ${post.title}, Text: ${post.content}');
  }
}

class Post {
  final String title;
  final String content;

  Post(this.title, this.content);
}

class SiteApi1 {
  String getSite1Posts() {
    return '[{"headline": "Title1", "text": "Sample text 1..."}]';
  }
}

class SiteApi2 {
  String getSite2Posts() {
    return '[{"headline": "Title2", "text": "Sample text 2..."}]';
  }
}

abstract class IPostsAPI {
  List<Post> getPosts();
}

class Site1Adapter implements IPostsAPI {
  final api = SiteApi1();
  @override
  List<Post> getPosts() {
    final rawPosts = jsonDecode(api.getSite1Posts()) as List;
    return rawPosts
        .map((post) => Post(post['headline'], post['text']))
        .toList();
  }
}

class Site2Adapter implements IPostsAPI {
  final api = SiteApi2();
  @override
  List<Post> getPosts() {
    final rawPosts = jsonDecode(api.getSite2Posts()) as List;
    return rawPosts
        .map((post) => Post(post['headline'], post['text']))
        .toList();
  }
}
