import 'dart:convert';
import 'dart:typed_data';

import 'package:pointycastle/export.dart';

void main(List<String> arguments) {
  String password = 'password'; // A week password

  var salt = createUint8ListFromString('Salt');
  var pkcs = KeyDerivator('SHA-1/HMAC/PBKDF2');
  var params = Pbkdf2Parameters(salt, 100, 16);

  pkcs.init(params);
  Uint8List key = pkcs.process(createUint8ListFromString(password));

  display('Key value', key);
}

Uint8List createUint8ListFromString(String value) =>
    Uint8List.fromList(utf8.encode(value));

void display(String title, Uint8List value) {
  print(title);
  print(value);
  print(base64.encode(value));
  print('==================');
}
