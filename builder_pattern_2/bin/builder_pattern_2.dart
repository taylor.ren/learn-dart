// See https://dart.academy/creational-design-patterns-for-dart-and-flutter-builder/
// Building models the Dart way
enum PizzaSize {
  S,
  M,
  L,
  XL,
}

enum PizzaSauce {
  none,
  marinara,
  garlic,
}

enum PizzaCrust {
  classic,
  deepDish,
}

class Pizza {
  final PizzaSize? size;
  final PizzaCrust? crust;
  final PizzaSauce? sauce;
  final List<String>? toppings;
  final bool? hasExtraCheese;
  final bool? hasDoubleMeat;
  final String? notes;

  @override
  String toString() {
    return 'Pizza (${size ?? "Any size"}), with ${crust ?? "No crust"}, ${sauce ?? "No sauce"} and ${toppings ?? "No toppings"}';
  }

  Pizza(
      {this.size,
      this.crust,
      this.sauce,
      this.toppings,
      this.hasExtraCheese = false,
      this.hasDoubleMeat = false,
      this.notes});

  Pizza copyWith(
      {PizzaSize? size,
      PizzaCrust? crust,
      PizzaSauce? sauce,
      List<String>? toppings,
      bool? hasExtraCheese,
      bool? hasDoubleMeat,
      String? notes}) {
    return Pizza(
        size: size ?? this.size,
        crust: crust ?? this.crust,
        sauce: sauce ?? this.sauce,
        toppings: toppings ?? this.toppings,
        hasExtraCheese: hasExtraCheese ?? this.hasExtraCheese,
        hasDoubleMeat: hasDoubleMeat ?? this.hasDoubleMeat,
        notes: notes ?? this.notes);
  }
}

void main(List<String> arguments) {
  Pizza p1 = Pizza(size: PizzaSize.L, crust: PizzaCrust.classic);

  Pizza p2 = p1.copyWith(toppings: ['roast beef', 'salmon'], crust: PizzaCrust.deepDish);

  print(p1);

  print(p2);
}
