import 'package:complex/complex.dart';

void main(List<String> arguments) {
  final z1 = Complex(1);
  final z2 = Complex(3, 4);

  print(z1.abs());
  print(z2.abs());

  print(z2.conjugate());
  print(z1 + z2);

  print(z2 * z2);
  print(z1 / z2);
}
