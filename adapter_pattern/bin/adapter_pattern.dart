// See https://dart.academy/structural-design-patterns-for-dart-and-flutter-adapter/
// As this article mentioned, Class Adapter requires multiple inheritance, which is not supported by Dart/Flutter
// So Object Adapter is demonstrated here.
void main(List<String> arguments) {
  OldRect oldRect = OldRect(100, 100, 20, 30);
  oldRect.draw();

  Rect rect = Rect(100, 100, 120, 130);
  rect.draw();
}

class OldRect {
  final int x;
  final int y;
  final int width;
  final int height;

  const OldRect(this.x, this.y, this.width, this.height);

  void draw() {
    print('Draw an oldRect');
  }
}

class Rect {
  late OldRect _oldRect;

  Rect(int left, int top, int right, int bottom) {
    _oldRect = OldRect(left, top, right - left, bottom - top);
  }

  void draw() => _oldRect.draw();
}
