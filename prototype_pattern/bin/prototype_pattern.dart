// See https://dart.academy/creational-design-patterns-for-dart-and-flutter-prototype/

class Point {
  final int x;
  final int y;

  const Point(this.x, this.y);

  Point copyWith({int? x, int? y}) {
    return Point(x ?? this.x, y ?? this.y);
  }

  Point clone() => copyWith(x: x, y: y);
}

void main(List<String> arguments) {
  final p1 = Point(5, 8);
  final p2 = p1.clone();

  print('${p1.x}, ${p1.y}');
  print('${p2.x}, ${p2.y}');

  assert(identical(p1, p2));
  //Not true! Program will stop in debug mode!

  final p3 = p1.copyWith(y: 3);
  print('${p3.x}, ${p3.y}');

  assert(identical(p1, p3));

  assert(1 == 4);
}
