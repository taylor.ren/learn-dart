import 'dart:async';

int counter = 0;
void main() {
  Duration d = Duration(seconds: 1);
  //Timer timer = Timer.periodic(d, timeout);
  Timer.periodic(d, timeout);
  print('Start ${getTime()}');
}

String getTime() {
  DateTime dt = DateTime.now();
  return dt.toString();
}

void timeout(Timer timer) {
  print('Timeout{$counter}: ${getTime()}');

  counter++;
  if (counter >= 5) timer.cancel();
}
