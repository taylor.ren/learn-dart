import 'dart:async';
import 'package:bloc/bloc.dart';

enum CounterEvent { inc, dec }

class BlocCounter extends Bloc<CounterEvent, int> {
  BlocCounter() : super(0);

  Stream<int> mapEventToState(CounterEvent event) async* {
    switch (event) {
      case CounterEvent.inc:
        yield state + 1;
        break;
      case CounterEvent.dec:
        yield state - 1;
        break;
    }
    throw UnimplementedError();
  }
}

Future<void> main() async {
  final bloc = BlocCounter();
  final streamSubscription = bloc.stream.listen((event) {
    print(event);
  });

  bloc.add(CounterEvent.inc);
  bloc.add(CounterEvent.inc);
}
