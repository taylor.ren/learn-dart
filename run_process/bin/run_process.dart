import 'dart:io';

void main() {
  var cmd = '';

  if (Platform.isWindows) {
    cmd = "C:\\Program Files\\Notepad++\\notepad++.exe";
  } else {
    cmd = 'ls -l';
  }

  Process.run(cmd, ['']).then((ProcessResult results) {
    print(results.stdout);
    print('='*10);

    print(results.exitCode);
  });
}
