class API{
  final String message;
  final String description;
  final String author;
  final String version;
  final String framework;

  const API({
    required this.message,
    required this.description,
    required this.author,
    required this.version,
    required this.framework,
  });

  factory API.fromJson(Map<String, dynamic> json) {
    return API(
      message: json['message'],
      description: json['description'],
      author: json['author'],
      version: json['version'],
      framework: json['framework'],
    );
  }
}
