import 'dart:convert';
import 'api.dart';

import 'package:http/http.dart' as http;

void main(List<String> arguments) async {
  var url = Uri.parse('https://api.rsywx.com/');
  var response = await http.get(url);

  print('Response status: ${response.statusCode}');
  //var res = api.fromJson(jsonDecode((response.body)));
  var res = jsonDecode(response.body)['data'];
  var api = API.fromJson(res);

  print(api.author);
}
