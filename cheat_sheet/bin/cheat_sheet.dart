String stringfy(int x, int y) {
  return "$x $y";
}

void printName(String fName, String lName, {String? suffix = ''}) {
  print('$fName $lName $suffix');
}

class MyDataObject {
  final int anInt;
  final String aString;
  final double aDouble;

  MyDataObject({
    this.anInt = 1,
    this.aString = 'Old',
    this.aDouble = 2.0,
  });

  MyDataObject copyWith({int? newInt, String? newString, double? newDouble}) {
    return MyDataObject(
      anInt: newInt ?? anInt,
      aString: newString ?? aString,
      aDouble: newDouble ?? aDouble,
    );
  }
}

void main() {
  print(stringfy(1, 3));
  print(stringfy(2, 3));

  String? foo = 'a string';
  String? bar;

  String? baz = foo ?? bar;
  print(baz);

  bar ??= 'a string';
  print(bar);

  print(upperCaseIt(bar));

  //Arrow syntax
  MyClass c = MyClass();
  print(c.product);
  c.incrementValue1();

  print(c.value1);

  print(c.joinWithCommas(['a', 'b', 'z']));

  BigObject o = BigObject();
  fillBigObject(o);
  print(o._done);

  //getter and setter
  ShoppingCart cart = ShoppingCart();
  cart.prices = [2, 3, 4];

  print('Total in cart: ${cart.total}');

  printName('Avinash', 'Gupta');
  printName('Poshmeister', 'Moneybuckets', suffix: 'IV');
}

BigObject fillBigObject(BigObject obj) {
  return obj
    ..aInt = 1
    ..aList = [1.2, 3.4, 5.6]
    ..aString = 'Consider it done'
    ..allDone();
}

class MyClass {
  int value1 = 2;
  int value2 = 3;
  int value3 = 5;

  int get product => value1 * value2 * value3;

  void incrementValue1() => value1++;

  String joinWithCommas(List<String> strings) => strings.join(", ");
}

String? upperCaseIt(String? str) {
  return str?.toUpperCase();
}

class BigObject {
  int aInt = 0;
  String aString = '';
  List<double> aList = [];
  bool _done = false;

  void allDone() {
    _done = true;
  }
}

class InvalidPriceException {}

class ShoppingCart {
  List<double> _prices = [];

  double get total => _prices.fold(0, (p, e) => p + e);
  set prices(List<double> values) {
    if (values.any((e) => e < 0)) {
      throw InvalidPriceException();
    }
    _prices = values;
  }
}
