// See https://dart.academy/streams-and-sinks-in-dart-and-flutter/

import 'dart:async';

class MyDataService {
  final _onNewData = StreamController<String>();
  Stream<String> get onNewData => _onNewData.stream;
}

void main(List<String> arguments) {
  final controller = StreamController<String>();

  controller.stream.listen((String data) => print(data));

  controller.sink.add('Data!');
  controller.add('Data 2');

  //controller.addError('Something terrible happened...');

  // Now more real world example

  final MyDataService service = MyDataService();

  service.onNewData.listen((String data) {
    print(data);
  }, onError: (error) {
    print(error);
  }, onDone: () {
    print('Stream is closed');
  });

  service._onNewData.sink.add('Data 3');
  service._onNewData.sink.add('Data 4!');

  // Please run to see the outcome. 
}
