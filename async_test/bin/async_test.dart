void main(List<String> arguments) async{
  print('Hello world!');
  await fetchOrder();
  print('Order Complete!');
}

Future<void> fetchOrder() async{
  return Future.delayed(const Duration(seconds: 2), () => print('Large Latte'));
}
