// Source: https://dart.academy/structural-design-patterns-for-dart-and-flutter-facade/
// On Facade design pattern
void main(List<String> arguments) {
  SmartHome sh = SmartHome();

  sh.out();
  print('I\'m leaving home.');
  print('*'*10);
  sh.home();
  print('I\'m at home now.');
}

class SmartHome {
  final SecuritySystem _securitySystem = SecuritySystem();
  final Intercom _intercom = Intercom();
  final WindowShades _windowShades = WindowShades();
  final Lights _lights = Lights();

  void home() {
    _securitySystem.disable();
    _intercom.on();
    _windowShades.open();
    _lights.on();
  }

  void out() {
    _securitySystem.enable();
    _intercom.off();
    _windowShades.close();
    _lights.off();
  }
}

class SecuritySystem {
  void enable() => print("SecuritySystem enabled");
  void disable() => print("SecuritySystem disabled");
}

class Intercom {
  void on() => print("Intercom ready");
  void off() => print("Intercom standing by");
}

class WindowShades {
  void open() => print("WindowShades open");
  void close() => print("WindowShades closed");
}

class Lights {
  void on() => print("Lights on");
  void off() => print("Lights off");
}
