import 'dart:io';
import 'dart:convert';

void main() async {
  var serverSock = await ServerSocket.bind('localhost', 3333);

  print('Listening...');

  await for (var socket in serverSock) {
    socket.listen((List<int> values) {
      print('${socket.remoteAddress} : ${socket.remotePort} = ${utf8.decode(values)}');
    });
  }
}
