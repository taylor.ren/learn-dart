// See https://dart.academy/structural-design-patterns-for-dart-and-flutter-flyweight/

import 'dart:typed_data';

void main(List<String> arguments) {
  final List<Enemy> enemies = [
    Enemy('Red Avenger'),
    Enemy('Red Avenger'),
    Enemy('Green Avenger'),
  ];

  for (var enemy in enemies) {
    enemy.moveTo(200, 100);
    enemy.draw();
  }
}

class EnemyType {
  final String name;
  final ByteData imageData;

  const EnemyType(this.name, this.imageData);
}

class Enemy {
  static final Map<String, EnemyType> types = {};

  final EnemyType type;
  int? x;
  int? y;

  Enemy(String typeName) : type = getType(typeName);

  void moveTo(int x, int y) {
    this.x = x;
    this.y = y;
  }

  void draw() {
    print('Drawing ${type.name} at ($x, $y)');
  }

  static EnemyType getType(String typeName) {
    return types.putIfAbsent(
        typeName, () => EnemyType(typeName, loadImageData(typeName)));
  }

  static ByteData loadImageData(String typeName) {
    return ByteData(200);
  }
}
