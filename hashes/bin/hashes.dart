import 'dart:convert';
import 'dart:typed_data';

import 'package:pointycastle/api.dart';

void main(List<String> arguments) {
  Digest digest = Digest("SHA-256");
  String value = '中文测试';

  Uint8List data = Uint8List.fromList(utf8.encode(value));
  Uint8List hashed = digest.process(data);

  print(hashed);

  print(base64.encode(hashed));
}
