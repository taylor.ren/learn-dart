import 'dart:convert';
import 'package:collection/collection.dart';
import 'dart:math';
import 'dart:typed_data';

import 'package:pointycastle/api.dart';

void main(List<String> arguments) {
  final keybytes = randomBytes(16);
  final key = KeyParameter(keybytes);

  final iv = randomBytes(8);
  final params = ParametersWithIV(key, iv);

  StreamCipher cipher = StreamCipher('Salsa20');
  cipher.init(true, params);

  //Encrypt
  String plain = 'Hello World';
  Uint8List plainData = createUint8ListFromString(plain);
  Uint8List encrypted = cipher.process(plainData);

  //Decrypt
  cipher.reset();
  cipher.init(false, params);
  Uint8List decrypt = cipher.process(encrypted);

  displayUnit8List('Plain text', plainData);
  displayUnit8List('Encrypted data', encrypted);
  displayUnit8List('Decrypted data', decrypt);

  Function eq = const ListEquality().equals;
  assert(eq(plain, decrypt));

  print(utf8.decode(decrypt));
}

Uint8List createUint8ListFromString(String value) =>
    Uint8List.fromList(utf8.encode(value));

void displayUnit8List(String title, Uint8List value) {
  print(title);
  print(value);
  print(base64.encode(value));
  print('----------------');
}

Uint8List randomBytes(int length) {
  final rnd = SecureRandom("AES/CTR/AUTO-SEED-PRNG");

  final key = Uint8List(16);
  final keyParam = KeyParameter(key);
  final params = ParametersWithIV(keyParam, Uint8List(16));

  rnd.seed(params);
  var random = Random();
  for (int i = 0; i < random.nextInt(255); i++) {
    rnd.nextUint8();
  }

  var bytes = rnd.nextBytes(length);
  return bytes;
}
