import 'package:ml_dataframe/ml_dataframe.dart';
import 'package:ml_preprocessing/ml_preprocessing.dart';
import 'package:ml_algo/ml_algo.dart';

void main(List<String> arguments) async {
  final samples = await fromCsv('./bin/diabetes.csv');
  final targetColumnName = 'Outcome';

  final splits = splitData(samples, [0.7]);
  final validationData = splits[0];
  final testData = splits[1];

  final validator = CrossValidator.kFold(validationData, numberOfFolds: 5);

  final createClassifier = (DataFrame samples) => LogisticRegressor(
      samples, targetColumnName,
      optimizerType: LinearOptimizerType.gradient,
      iterationsLimit: 90,
      learningRateType: LearningRateType.timeBased,
      batchSize: samples.rows.length,
      probabilityThreshold: 0.7,
      collectLearningData: true);

  final scores =
      await validator.evaluate(createClassifier, MetricType.accuracy);

  final accuracy = scores.mean();

  print('Accuracy on k fold validation ${accuracy.toStringAsFixed(2)}');

  final testSplits = splitData(testData, [0.8]);
  final classifier = createClassifier(testSplits[0]);
  final finalScore = classifier.assess(testSplits[1], MetricType.accuracy);

  print('Final score ${finalScore.toStringAsFixed(2)}');

  print('Cost per interation: ${classifier.costPerIteration}');

  await classifier.saveAsJson('./bin/classifier.json');
}
