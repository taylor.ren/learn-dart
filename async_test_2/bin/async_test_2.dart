Future<void> printOrder() async {
  print('Awaiting Order...');
  var order = await fetchOrder();
  print('You ordered $order');
}

Future<String> fetchOrder() {
  return Future.delayed(const Duration(seconds: 4), () => 'Large Latte');
}

void main() async {
  countSeconds(6);
  await printOrder();
}

void countSeconds(int s) {
  for (var i = 1; i <= s; i++) {
    Future.delayed(Duration(seconds: i), () => print(i));
  }
}
