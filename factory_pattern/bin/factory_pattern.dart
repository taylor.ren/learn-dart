// See https://dart.academy/creational-design-patterns-for-dart-and-flutter-factory-method/

enum ShapeType {
  triangle,
  rectangle,
}

abstract class Shape {
  factory Shape(ShapeType type) {
    switch (type) {
      case ShapeType.triangle:
        return Triangle();
      case ShapeType.rectangle:
        return Rectangle();
      default:
        throw ('Unknown Type');
    }
  }

  void draw();
}

class Triangle implements Shape {
  @override
  void draw() {
    print('TRIANGLE');
  }
}

class Rectangle implements Shape {
  @override
  void draw() {
    print('RECTANGLE');
  }
}

void main(List<String> arguments) {
  final shape1 = Shape(ShapeType.rectangle);
  final shape2 = Shape(ShapeType.triangle);

  shape1.draw();
  shape2.draw();
}
