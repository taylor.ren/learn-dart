//See https://dart.academy/creational-design-patterns-for-dart-and-flutter-singleton/

class Singleton {
  static Singleton? _instance;

  factory Singleton() => _instance ?? Singleton._internal();

  Singleton._internal() {
    _instance = this;
  }
}

void main(List<String> arguments) {
  Singleton sg1 = Singleton();
  print(sg1);

  Singleton sg2 = Singleton();
  print(sg2);

  assert(identical(sg1, sg2));
}
