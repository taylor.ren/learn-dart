import 'dart:io';

void main() async {
  var socket = await Socket.connect('localhost', 3333);
  print('Connected');
  socket.write('你好啊！');
  socket.write('Hello, world!');
  
  print('Sent, closing');
  await socket.close();
  print('Closed');
  exit(0);
}
