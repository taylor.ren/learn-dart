import 'dart:io';
import 'dart:async';

void main() {
  String path = Directory.current.path + '/test.txt';
  print('Appending to $path');

  File file = File(path);

  Future<RandomAccessFile> f = file.open(mode: FileMode.append);

  f.then((RandomAccessFile raf) {
    print('File has been opened!');

    raf.writeString('Hello world! ${DateTime.now()}\n')
    .then((value) {
      print('File appended');
    }).whenComplete(() => raf.close())
    .catchError(()=>print("Error!"));
  });
}
